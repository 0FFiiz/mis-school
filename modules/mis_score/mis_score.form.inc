<?php

function input_teacherId_form($form, &$form_state) {
    global $user;
    
    $form['tid'] = array(
        '#type' => 'textfield',
        '#title' => 'รหัศอาจารย์',
        '#default_value' => $user->name,
        '#disabled' => TRUE
    );
    
    $form['term'] = array(
        '#type' => 'textfield',
        '#title' => 'เทอม',
    );
    $form['year'] = array(
        '#type' => 'textfield',
        '#title' => 'ปีการศึกษา'
    );


    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit'
    );
    return $form;
}

function input_teacherId_form_submit($form, &$form_state) {
    global $user;
    $tid = $user->name;
    dsm($user->name);
    $term = $form_state['values']['term'];
    $year = $form_state['values']['year'];
    drupal_goto("mis/set-subject-form/".$term . "/" . $year);
}

function get_score_detail_form($form, &$form_state, $term, $year) {
    global $user;

    $form['id'] = array(
        '#type' => 'select',
        '#options' => get_teach_subject($user->name, $term, $year),
        '#title' => 'รายวิชาที่สอน',
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'ค้นหา'
    );
    return $form;
}

function get_score_detail_form_submit($form, &$form_state) {
    $id = $form_state['values']['id'];
    
    drupal_goto("mis/get-score-detail/".$id);
    
    
}


function input_teacherId_printscore_form($form, &$form_state) {

    
    $form['term'] = array(
        '#type' => 'textfield',
        '#title' => 'เทอม'
    );
    $form['year'] = array(
        '#type' => 'textfield',
        '#title' => 'ปีการศึกษา'
    );
    
    $form['id'] = array(
        '#type' => 'select',
        '#options' => get_teach_subject($form_state['values']['tid'], $form_state['values']['term'], $form_state['values']['year']),
        '#title' => 'รายวิชาที่สอน',
    );


    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit'
    );
    return $form;
}

function input_teacherId_printscore_form_submit($form, &$form_state) {
    dsm($form_state);
}