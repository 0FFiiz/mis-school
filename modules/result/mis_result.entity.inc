<?php

class TblScoreEntity extends Entity {
    
}

class TblBehaviorEntity extends Entity {
    
}

class TblDetailScoreEntity extends Entity {
    
}

class TblDetailScoreDetailEntity extends Entity {
    
}

class TblStudentIncompleteDetailEntity extends Entity {
    
}

class TblStudentInCompleteMainEntity extends Entity {
    
}

class TblStudentNoTestEntity extends Entity {
    
}

class TblScoreViewsController extends EntityDefaultViewsController{

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'result';
        }
        return $data;
    }
    protected function schema_fields() {
        $data = parent::schema_fields();
        /*
        $data['StdNo']['relationship'] = array(
            'label' => t('StdNo'),
            'base' => 'tblStudentNumber',
            'field' => 'StdNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'Tabain.dbo.tblStudentNumber',
        );
        */
        $data['SubNo']['relationship'] = array(
        		'label' => t('Subject'),
        		'base' => 'tblSubject',
        		'field' => 'SubNo',
        		'handler' => 'views_handler_relationship',
        		'table formula' => 'ViChaKarn.dbo.tblSubject',
        );
        $data['StdNo']['relationship'] = array(
        		'label' => t('StudentRoom'),
        		'base' => 'tblStudentRoom',
        		'field' => 'StdNo',
        		'handler' => 'views_handler_relationship',
        		'table formula' => 'Tabain.dbo.tblStudentRoom',
        );
        return $data;

    }
 
}

class TblBehaviorViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblDetailScoreViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblDetailScoreDetailViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblStudentIncompleteDetailViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblStudentInCompleteMainViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblStudentNoTestViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}
?>






