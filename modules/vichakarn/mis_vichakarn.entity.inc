<?php

class TblTimeTableEntity extends Entity{}
class TblTeachEntity extends Entity{}
class TblTypeTeachEntity extends Entity{}
class TblSubjectEntity extends Entity{}

class TblTimeTableViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
				$table = $this->info['base table'];
				$data[$table]['table']['base']['database'] = 'vichakarn';
		}
		return $data;
	}
	protected function schema_fields() {
		$data = parent::schema_fields();
		$data['TeacherNo']['relationship'] = array(
			'label' => t('Teacher'),
			'base' => 'tblTeacherHis',
			'field' => 'TeacherNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'Personal.dbo.tblTeacherHis',
		);
		$data['SubNo']['relationship'] = array(
			'label' => t('Subject'),
			'base' => 'tblSubject',
			'field' => 'SubNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'ViChaKarn.dbo.tblSubject',
		);
		return $data;
	}
}

class TblTeachViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'vichakarn';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		$data['SubNo']['relationship'] = array(
			'label' => t('Subject'),
			'base' => 'tblSubject',
			'field' => 'SubNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'vichakarn.dbo.tblSubject',
		);
		$data['TeacherNo']['relationship'] = array(
			'label' => t('Teacher'),
			'base' => 'tblTeacherHis',
			'field' => 'TeacherNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'Personal.dbo.tblTeacherHis',
		);
		$data['LavelNo']['relationship'] = array(
			'label' => t('Lavel'),
			'base' => 'tblLavel',
			'field' => 'LavelNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'BaseData.dbo.tblLavel',
		);	
		$data['DepartNo']['relationship'] = array(
			'label' => t('Depart'),
			'base' => 'tblDepart',
			'field' => 'DepartNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'BaseData.dbo.tblDepart',
		);	
		$data['RoundNo']['relationship'] = array(
			'label' => t('Round'),
			'base' => 'tblRound',
			'field' => 'RoundNo',
			'handler' => 'views_handler_relationship',
			'table formula' => 'BaseData.dbo.tblRound',
		);		
		return $data;
	}
}

class TblTypeTeachViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'vichakarn';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		return $data;
	}
}

class TblSubjectViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'vichakarn';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		return $data;
	}
}



