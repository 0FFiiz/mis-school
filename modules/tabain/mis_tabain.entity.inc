<?php

class TblRegisterEntity extends Entity {
    
}

class TblStudentEntity extends Entity {
    
}

class TblStudentNumberEntity extends Entity {
    
}

class TblClassEntity extends Entity {
    
}

class TblStudentRoomEntity extends Entity {
    
}

class TblRegisterViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'tabain';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        /*
          $data['StdNo']['relationship'] = array(
          'label' => t('StudentNumber'),
          'base' => 'tblStudentNumber',
          'field' => 'StdNo',
          'handler' => 'views_handler_relationship',
          'table formula' => 'Tabain.dbo.tblStudentNumber',
          );
         */

        $data['StdNo']['relationship'] = array(
            'label' => t('StudentRoom'),
            'base' => 'tblStudentRoom',
            'field' => 'StdNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'Tabain.dbo.tblStudentRoom',
        );
        return $data;
    }

}

class TblStudentViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'tabain';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        /*
          $data['StdNo']['relationship'] = array(
          'label' => t('StudentRoom'),
          'base' => 'tblStudentRoom',
          'field' => 'StdNo',
          'handler' => 'views_handler_relationship',
          'table formula' => 'Tabain.dbo.tblStudentRoom',
          );
         */
        return $data;
    }

}

class TblStudentNumberViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'tabain';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['StdNoRef']['relationship'] = array(
            'label' => t('StudentNoRef'),
            'base' => 'tblStudent',
            'field' => 'StdNoRef',
            'handler' => 'views_handler_relationship',
            'table formula' => 'Tabain.dbo.tblStudent',
        );
        return $data;
    }

}

class TblClassViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'vichakarn';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblStudentRoomViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'tabain';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['StdNo']['relationship'] = array(
            'label' => t('StudentNumber'),
            'base' => 'tblStudentNumber',
            'field' => 'StdNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'Tabain.dbo.tblStudentNumber',
        );
        /*
        $data['LavelNo']['relationship'] = array(
            'label' => t('LavelNo'),
            'base' => 'tblTeach',
            'field' => 'LavelNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'ViChaKarn.dbo.tblTeach',
        );
        $data['DepartNo']['relationship'] = array(
            'label' => t('DepartNo'),
            'base' => 'tblTeach',
            'field' => 'DepartNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'ViChaKarn.dbo.tblTeach',
        );
        $data['RoundNo']['relationship'] = array(
            'label' => t('RoundNo'),
            'base' => 'tblTeach',
            'field' => 'RoundNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'ViChaKarn.dbo.tblTeach',
        );
        $data['Year']['relationship'] = array(
            'label' => t('Year'),
            'base' => 'tblTeach',
            'field' => 'Year',
            'handler' => 'views_handler_relationship',
            'table formula' => 'ViChaKarn.dbo.tblTeach',
        );
        $data['Room']['relationship'] = array(
            'label' => t('Room'),
            'base' => 'tblTeach',
            'field' => 'Room',
            'handler' => 'views_handler_relationship',
            'table formula' => 'ViChaKarn.dbo.tblTeach',
        );
       */
        return $data;
    }

}

