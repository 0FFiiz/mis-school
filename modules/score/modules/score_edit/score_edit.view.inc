<?php

function save_edit_score_form($form, &$form_state) {
    $output = get_detail_score('T50008', '1', '2554', '2201-2306');

    $head = array(t('เลขที่'), t('รหัสนักศึกษา'), t('ชื่อ - สกุล'));
    $i = 0;
    foreach ($output as $item) {
        if ($item->WellNo != 'F') {
            $head[] = t($item->WellNo . '(' . $item->Score . ')');
            $i++;
        }
    }
    $n=$i;
    for ($i; $i < 10; $i++) {
        $head[] = t('');
    }
    $head[] = t('รวม');
    $head[] = t('ปภ.');
    $head[] = t('รวมทั้งสิ้น');
    $head[] = t('หมายเหตุ');

    $output = get_header_std_score();
    //dd($output);

    $form = render_save_edit_score_table('score', $head, $output, $n);

    return $form;
}

function render_save_edit_score_table($name = '', $head = array(), $content = array(), $numScore=0) {



    $form[$name] = array(
        '#prefix' => '<div id="' . $name . '">',
        '#suffix' => '</div>',
        '#tree' => TRUE,
        '#theme' => 'table',
        '#header' => $head,
        '#rows' => array(),
    );
    for ($i = 0; $i < count($content); $i++) {

        // Build the fields for this row in the table. We'll be adding
        // these to the form several times, so it's easier if they are
        // individual variables rather than in an array.

        $num = array(
            '#type' => 'item',
            '#title' => $i,
        );
        $stdNo = array(
            '#type' => 'item',
            '#title' => $content[$i]['StdNo'],
        );
        $stdFullName = array(
            '#type' => 'item',
            '#title' => $content[$i]['StdNo'],
        );
        $S1 = array(
            '#type' => $numScore>=1 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S1'] ? $content[$i]['S1'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score1'))
        );
        $S2 = array(
            '#type' => $numScore>=2 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S2'] ? $content[$i]['S2'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score2'))
        );
        $S3 = array(
            '#type' => $numScore>=3 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S3'] ? $content[$i]['S3'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score3'))
        );
        $S4 = array(
            '#type' => $numScore>=4 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S4'] ? $content[$i]['S4'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score4'))
        );
        $S5 = array(
            '#type' => $numScore>=5 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S5'] ? $content[$i]['S5'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score5'))
        );
        $S6 = array(
            '#type' => $numScore>=6 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S6'] ? $content[$i]['S6'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score6'))
        );
        $S7 = array(
            '#type' => $numScore>=7 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S7'] ? $content[$i]['S7'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score7'))
        );
        $S8 = array(
            '#type' => $numScore>=8 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S8'] ? $content[$i]['S8'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score8'))
        );
        $S9 = array(
            '#type' => $numScore>=9 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S9'] ? $content[$i]['S9'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score9'))
        );
        $S10 = array(
            '#type' => $numScore>=10 ? 'textfield' : 'item',
            '#title' => '',
            '#default_value' => $content[$i]['S10'] ? $content[$i]['S10'] : '0',
            '#size' => 1,
            '#attributes' => array('id' => array('score10'))
        );
        $sTotal = array(
            '#type' => 'item',
            '#title' => $content[$i]['sTotal'],
            '#attributes' => array('id' => array('sTotal'))
        );
        $fi = array(
            '#type' => 'item',
            '#title' => $content[$i]['Fi'],
            '#attributes' => array('id' => array('fi'))
        );
        $sSum = array(
            '#type' => 'item',
            '#title' => $content[$i]['sSum'],
            '#attributes' => array('id' => array('sSum'))
        );
        $memo = array(
            '#type' => 'item',
            '#title' => $content[$i]['sSum'],
            '#attributes' => array('id' => array('memo'))
        );


        // Include the fields so they'll be rendered and named
        // correctly, but they'll be ignored here when rendering as
        // we're using #theme => table.
        //
      // Note that we're using references to the variables, not just
        // copying the values into the array.

        $form[$name][] = array(
            'stdNo' => &$num,
            'stdNo' => &$stdNo,
            'stdFullName' => &$stdFullName,
            'S1' => &$S1,
            'S2' => &$S2,
            'S3' => &$S3,
            'S4' => &$S4,
            'S5' => &$S5,
            'S6' => &$S6,
            'S7' => &$S7,
            'S8' => &$S8,
            'S9' => &$S9,
            'S10' => &$S10,
            'sTotal' => &$sTotal,
            'fi' => &$fi,
            'sSum' => &$sSum,
            '$memo' => &$memo,
        );

        // Now add references to the fields to the rows that
        // `theme_table()` will use.
        //
      // As we've used references, the table will use the very same
        // field arrays as the FAPI used above.

        $form[$name]['#rows'][] = array(
            array('data' => &$num),
            array('data' => &$stdNo),
            array('data' => &$stdFullName),
            array('data' => &$S1),
            array('data' => &$S2),
            array('data' => &$S3),
            array('data' => &$S4),
            array('data' => &$S5),
            array('data' => &$S6),
            array('data' => &$S7),
            array('data' => &$S8),
            array('data' => &$S9),
            array('data' => &$S10),
            array('data' => &$sTotal),
            array('data' => &$fi),
            array('data' => &$sSum),
            array('data' => &$memo),
        );

        // Because we've used references we need to `unset()` our
        // variables. If we don't then every iteration of the loop will
        // just overwrite the variables we created the first time
        // through leaving us with a form with 3 copies of the same fields.

        unset($num);
        unset($stdNo);
        unset($stdFullName);
        unset($S1);
        unset($S2);
        unset($S3);
        unset($S4);
        unset($S5);
        unset($S6);
        unset($S7);
        unset($S8);
        unset($S9);
        unset($S10);
        unset($sTotal);
        unset($fi);
        unset($sSum);
        unset($memo);
    }

    return $form;
}