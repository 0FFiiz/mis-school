<?

class TblStdLearnInClassMainEntity extends Entity {
    
}

class TblStdLearnInClassDetailEntity extends Entity {
    
}

class TblStdLearnInClassMainViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'government';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['SLICNo']['relationship'] = array(
            'label' => t('StdLearnInClassDetail'),
            'base' => 'tblStdLearnInClassDetail',
            'field' => 'SLICNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'GovernMent.dbo.tblStdLearnInClassDetail',
        );
        return $data;
    }

}

class TblStdLearnInClassDetailViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'government';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['StdNo']['relationship'] = array(
            'label' => t('StudentRoom'),
            'base' => 'tblStudentRoom',
            'field' => 'StdNo',
            'handler' => 'views_handler_relationship',
            'table formula' => 'Tabain.dbo.tblStudentRoom',
        );
        return $data;
    }

}

