<?php

class TblDegreeEntity extends Entity {
    
}

class TblDegreeStudyEntity extends Entity {
    
}

class TblDepartEntity extends Entity {
    
}

class TblLavelEntity extends Entity {
    
}

class TblPrefixEntity extends Entity {
    
}

class TblRoundEntity extends Entity {
    
}

class TblDegreeViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblDegreeStudyViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblDepartViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblLavelViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblPrefixViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class TblRoundViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'basedata';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

