<?php

function mis_class_attend_check_name_form($form, &$form_state, $SubNo, $ClassNo, $TimeNo) {

    /*
      $header = array('ลำดับ','รหัส','ชื่อ','นามสกุล','');
      $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $SubNo,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      );
      //mis_class_attend_connect_other_database('Tabain');
      db_set_active();
      $data = mis_tabain_class_register_subject($SubNo,$ClassNo);
      dsm($data);
      $i=1;
      $active = array(
      0 => t('มาทัน'),
      1 => t('มาสาย'),
      2 => 'ลา',
      3 => 'ขาด'
      );
      $form['people'] = array(
      '#prefix' => '<div id="people">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => array(),
      );

      foreach($data as $line){
      $check = array(
      '#id' => 'people-' . $i . '-fname',
      '#type' => 'radios',
      //'#title' => t('Poll status'),
      '#default_value' => 1,
      '#options' => $active,);
      $form['people'][$line['StdNo']] = array(
      'check' => &$check,
      );
      $form['people']['#rows'][] = array(
      array('data' => $i++),
      array('data' => $line['StdNo']),
      array('data' => $line['Fname']),
      array('data' => $line['Lname']),
      array('data' => &$check)
      );
      unset($check);
      $form_state['StdNo'][]=$line['StdNo'];
      }

      //dsm($form);
      unset($form['check']);
      $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
     */
    $i = 1;
    $data = mis_tabain_class_register_subject($SubNo, $ClassNo);
    //$form_state['TeacherNo'] = misschool_account_load()->TeacherNo;
    $form_state['TeacherNo'] = 'T45004';
    $form_state['sTerm'] = misschool_settings("semester");
    $form_state['sYear'] = misschool_settings("acadyear");
    $form_state['TimeNo'] = $TimeNo;
    $form_state['SubNo'] = $SubNo;
    $form_state['ClassNo'] = $ClassNo;
    //$form['#attend'] = $attend_load;
    //reset($class_timetable);
    $options = array();
    $header = array
        (
        'no' => t('ลำดับ'),
        'student_code' => t('รหัสนักศึกษา'),
        'student_name' => t('ชื่อ'),
        'student_surname' => t('นามสกุล'),
    );
    //dsm($data);
    foreach ($data as $student) {
        $options[$i] = array(
            'no' => $i,
            'student_code' => $student['StdNo'],
            'student_name' => $student['Fname'],
            'student_surname' => $student['Lname'],
            'value' => 1,
        );
        $form_state['StdNo'][$i] = $student['StdNo'];
        $i++;
    }
    $choice = array(1 => 'ตรงเวลา', 2 => 'ขาด', 2 => 'ลา', 3 => 'ป่วย');
    $form['table'] = array(
        '#type' => 'tableradioselect',
        '#header' => $header,
        '#options' => $options,
        '#radios' => array(
            '#header' => $choice,
            '#options' => $choice,
        ),
    );
    //$form = theme_course_type_form($form1);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
    return $form;
}

/*
  function mis_class_attend_check_name_form_validate($form,&$form_state){
  dsm($form_state);
  }
 */

function mis_class_attend_check_name_form_submit($form, &$form_state) {
    
    $result = save_class_chk_name($form_state);
    
    $SlicNo = $form_state['ClassNo'] . '-' . date("Ymd");
    $TimeNo = $form_state['TimeNo'];
    $TermYear = $form_state['sTerm'] . '/' . $form_state['sYear'];
    $NameEdit = get_teacher_name($form_state['TeacherNo']);
    $SubNo = $form_state['SubNo'];
     
    //dsm($SubNo);

    drupal_goto('class/debug-attend-result/' . $SlicNo . '/' . $SubNo . '/' . $TimeNo);
}

function mis_class_attend_check_name_form_callback($form, &$form_state) {
    dsm($form_state);
    $element = $form['box'];
    $element['#markup'] = "Clicked submit (";
    $form_state['rebuild'] = TRUE;
    return $element;
}

