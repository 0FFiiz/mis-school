<?php

class TblTeacherInfoEntity extends Entity{}
class TblTeacherHisEntity extends Entity{}

class TblTeacherInfoViewsController extends EntityDefaultViewsController{
	protected function schema_fields() {
		$data = parent::schema_fields();
		return $data;
	}
	
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'personal';
		}
		return $data;
	}
}

class TblTeacherHisViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'personal';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		return $data;
	}
}
