<?php

function misschool_admin_settings() {
    $form = array();
    $form['misschool_database_server'] = array(
        '#type' => 'textfield',
        '#title' => 'Database Server',
        '#default_value' => variable_get('misschool_database_server', '192.168.0.86'),
        '#required' => TRUE,
    );

    $form['misschool_acadyear'] = array(
        '#type' => 'textfield',
        '#title' => t('ปีการศึกษา'),
        '#default_value' => variable_get('misschool_acadyear', '2554'),
        '#required' => TRUE,
    );

    $form['misschool_semester'] = array(
        '#type' => 'textfield',
        '#title' => t('เทอม'),
        '#default_value' => variable_get('misschool_semester', '1'),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}

function misschool_admin_import() {
    $form = array();

    $form['submit'] = array(
        '#type' => 'submit',
        '#weight' => 3,
        '#value' => t('Start Import'),
    );

    return $form;
}

function misschool_admin_import_submit() {

    $school_id = db_select("[BaseData].[dbo].[tblSchoolInfo]", "s")
                    ->fields("s", array("SchoolNo8"))
                    ->execute()->fetchField(0);

    $teacher_role = user_role_load_by_name('teacher');
    if (!$teacher_role) {

        $role = new stdClass();
        $role->name = 'teacher';
        // $role->weight = 10;
        user_role_save($role);


        // Role to grant the permissions to
        $teacher_role = user_role_load_by_name('teacher');
        $teacher_rid = $teacher_role->rid;
        // Define our 'editor' role permissions
// 	$editor_permissions = array(
// 			'administer blocks' => TRUE, // Grant permission
// 			'access dashboard' => FALSE, // Revoke permission
// 	);
        // Grant permissions to our 'editor' role
// 	user_role_change_permissions($teacher_rid, $editor_permissions);
    }

    // Determine the roles of our new user
    $new_user_roles = array(
        DRUPAL_AUTHENTICATED_RID => 'authenticated user',
        $teacher_role->rid => TRUE,
    );

    $result = db_select('[Personal].[dbo].[tblUserName]', 'u')
            ->fields('u')
            ->condition('IsNormal', 1)
            ->execute();

    while ($record = $result->fetchAssoc()) {
        // Create a new user
        try {
            $new_user = new stdClass();
            $new_user->name = $record['UserName'];
            $new_user->pass = $record['Password']; // plain text, hashed later
            $new_user->mail = $record['TeacherNo'];
            $new_user->roles = $new_user_roles;
            $new_user->status = 1; // omit this line to block this user at creation
            $new_user->is_new = TRUE; // not necessary because we already omit $new_user->uid		
            $account = drupal_anonymous_user();
            $account = user_save($account, (array) $new_user);
        } catch (Exception $ex) {
            
        }
    }

    drupal_set_message("Import done.");
}
